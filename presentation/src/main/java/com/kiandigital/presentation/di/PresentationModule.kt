package com.kiandigital.presentation.di

import com.kiandigital.presentation.viewmodel.MovieViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel {
        MovieViewModel(
            getPopularMovieUseCase = get(),
            loadMorePopularMovieUseCase = get(),
            getTopRatedMovieUseCase = get(),
            loadMoreTopRatedMovieUseCase = get(),
            getUpcomingMovieUseCase = get(),
            loadMoreUpcomingMovieUseCase = get()
        )
    }
}