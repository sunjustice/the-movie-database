package com.kiandigital.presentation.viewmodel

import androidx.lifecycle.liveData
import com.kiandigital.domain.action.MoviesAction
import com.kiandigital.domain.result.MovieResult
import com.kiandigital.domain.usecase.*
import com.kiandigital.presentation.state.MovieViewState
import timber.log.Timber

class MovieViewModel(
    private val getPopularMovieUseCase: GetPopularMovieUseCase,
    private val loadMorePopularMovieUseCase: LoadMorePopularMovieUseCase,
    private val getTopRatedMovieUseCase: GetTopRatedMovieUseCase,
    private val loadMoreTopRatedMovieUseCase: LoadMoreTopRatedMovieUseCase,
    private val getUpcomingMovieUseCase: GetUpcomingMovieUseCase,
    private val loadMoreUpcomingMovieUseCase: LoadMoreUpcomingMovieUseCase
) : BaseViewModel<MovieViewState, MoviesAction, MovieResult>(MovieViewState()) {

    /**
     * This is a [BaseViewModel] method we have to implement in every ViewModel.
     * It's the first step, when the ViewModel receives an action (intent)
     * The purpose is to call the proper use case for the required action.
     * The LiveData block will automatically execute when the LiveData becomes active (just after an action),
     * and it will change its value when the use case calls emit()
     */

    override fun handle(action: MoviesAction) = liveData<MovieResult> {
        Timber.i("Handle: $action")
        when (action) {
            is MoviesAction.GetPopularMoviesAction -> getPopularMovieUseCase.execute(this)
            is MoviesAction.LoadMorePopularMoviesAction -> loadMorePopularMovieUseCase.execute(this)
            is MoviesAction.GetTopRatedMoviesAction -> getTopRatedMovieUseCase.execute(this)
            is MoviesAction.LoadMoreTopRatedMoviesAction -> loadMoreTopRatedMovieUseCase.execute(
                this
            )
            is MoviesAction.GetUpcomingMoviesAction -> getUpcomingMovieUseCase.execute(this)
            is MoviesAction.LoadMoreUpcomingMoviesAction -> loadMoreUpcomingMovieUseCase.execute(
                this
            )
        }
    }

    /**
     * This is a [BaseViewModel] method we have to implement in every ViewModel.
     * It's the second step, when we receive the result of the use case and the last view state.
     * The purpose is to create and return the new view state
     */
    override fun reduce(currentViewState: MovieViewState, result: MovieResult): MovieViewState {
        Timber.i("currentViewState: $currentViewState")
        Timber.i("Reduce: $result")
        return when (result) {
            is MovieResult.Loading -> currentViewState.copy(isLoading = true)
            is MovieResult.LoadingMore -> currentViewState.copy(
                isLoading = false,
                isLoadingMore = true
            )
            is MovieResult.Success -> currentViewState.copy(
                isLoading = false,
                isLoadingMore = false,
                movies = result.movies,
                error = null
            )
            is MovieResult.Failure -> currentViewState.copy(
                isLoading = false,
                isLoadingMore = false,
                error = Throwable(result.errorMessage)
            )
        }
    }


}