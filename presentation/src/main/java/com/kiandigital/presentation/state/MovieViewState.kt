package com.kiandigital.presentation.state

import com.kiandigital.domain.models.Movie

data class MovieViewState(
    val isLoading: Boolean = false,
    val isLoadingMore: Boolean = false,
    val movies: List<Movie> = emptyList(),
    val error: Throwable? = null
): ViewState