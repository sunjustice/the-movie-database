package com.kiandigital.data.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.kiandigital.data.BuildConfig
import com.kiandigital.data.repository.TmdbRepository
import com.kiandigital.data.source.remote.TmdbRemoteSource
import com.kiandigital.data.source.remote.TmdbRemoteSourceContract
import com.kiandigital.data.source.remote.api.TmdbService
import com.kiandigital.domain.repository.TmdbRepositoryContract
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_API_URL = "https://api.themoviedb.org/3/"

val dataModule = module {
    single { provideOkHttpClient() }
    single { provideRetrofit(client = get()) }
    single { provideMoviesService(retrofit = get()) }
    single<TmdbRemoteSourceContract> { TmdbRemoteSource(tmdbService = get()) }
    single<TmdbRepositoryContract> { TmdbRepository(movieRemoteSource = get()) }
}

fun provideOkHttpClient(): OkHttpClient = OkHttpClient().newBuilder()
    .addInterceptor(HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
        else HttpLoggingInterceptor.Level.NONE
    })
    .build()

fun provideRetrofit(client: OkHttpClient): Retrofit = Retrofit.Builder()
    .baseUrl(BASE_API_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .client(client)
    .build()

fun provideMoviesService(retrofit: Retrofit): TmdbService = retrofit.create(TmdbService::class.java)