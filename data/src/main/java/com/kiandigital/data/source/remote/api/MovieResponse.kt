package com.kiandigital.data.source.remote.api

import com.kiandigital.domain.models.Movie

class MovieResponse {
    val page: Int = 0
    val results: List<Movie>? = null
}