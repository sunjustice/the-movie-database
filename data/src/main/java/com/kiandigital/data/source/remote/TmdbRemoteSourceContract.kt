package com.kiandigital.data.source.remote

import com.kiandigital.data.source.remote.api.MovieResponse

interface TmdbRemoteSourceContract {
    suspend fun getPopularMoviesFromApi(page: Int = 1): MovieResponse?
    suspend fun getTopRatedMoviesFromApi(page: Int = 1): MovieResponse?
    suspend fun getUpcomingMoviesFromApi(page: Int = 1): MovieResponse?
}