package com.kiandigital.data.source.remote

import com.kiandigital.data.source.remote.api.MovieResponse
import com.kiandigital.data.source.remote.api.TmdbService

class TmdbRemoteSource(private val tmdbService: TmdbService) : TmdbRemoteSourceContract {

    override suspend fun getPopularMoviesFromApi(page: Int): MovieResponse? {
        val response = tmdbService.getPopularMoviesAsync(page = page)
        return response.body()
    }

    override suspend fun getTopRatedMoviesFromApi(page: Int): MovieResponse? {
        val response = tmdbService.getTopRatedMoviesAsync(page = page)
        return response.body()
    }

    override suspend fun getUpcomingMoviesFromApi(page: Int): MovieResponse? {
        val response = tmdbService.getUpcomingMoviesAsync(page = page)
        return response.body()
    }

}