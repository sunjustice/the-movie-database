package com.kiandigital.data.repository

import com.kiandigital.data.source.remote.api.MovieResponse
import com.kiandigital.domain.models.Movie
import timber.log.Timber

abstract class BaseTmdbRepository {

    protected fun getMovies(
        loadMore: Boolean,
        movies: MutableList<Movie>,
        movieResponse: MovieResponse?,
        onPageRetrieved: (actualPage: Int) -> Unit
    ): List<Movie> {
        if (loadMore || movies.isEmpty()) {
            movieResponse?.let { response ->
                response.results?.let { movies.addAll(it.toMutableList()) }
                Timber.i("Page ${response.page} retrieved. Movies size: ${movies.size}")
                onPageRetrieved(response.page)
            }
        }
        return movies.toList()
    }
}