package com.kiandigital.data.repository

import com.kiandigital.data.source.remote.TmdbRemoteSourceContract
import com.kiandigital.domain.models.Movie
import com.kiandigital.domain.repository.TmdbRepositoryContract

class TmdbRepository(
    private val movieRemoteSource: TmdbRemoteSourceContract
) : BaseTmdbRepository(), TmdbRepositoryContract {

    /**
     * Repository cache
     */
    private var popularMovies = mutableListOf<Movie>()
    private var popularNextPage: Int = 1
    private var topRatedMovies = mutableListOf<Movie>()
    private var topRatedNextPage: Int = 1
    private var upcomingMovies = mutableListOf<Movie>()
    private var upcomingNextPage: Int = 1

    override suspend fun getPopularMoviesAsync(loadMore: Boolean): List<Movie>? {
        return getMovies(
            loadMore,
            popularMovies,
            movieRemoteSource.getPopularMoviesFromApi(popularNextPage)
        ) {
            popularNextPage = it + 1
        }
    }

    override suspend fun getTopRatedMoviesAsync(loadMore: Boolean): List<Movie>? {
        return getMovies(
            loadMore,
            topRatedMovies,
            movieRemoteSource.getTopRatedMoviesFromApi(topRatedNextPage)
        ) {
            topRatedNextPage = it + 1
        }
    }

    override suspend fun getUpcomingMoviesAsync(loadMore: Boolean): List<Movie>? {
        return getMovies(
            loadMore,
            upcomingMovies,
            movieRemoteSource.getUpcomingMoviesFromApi(upcomingNextPage)
        ) {
            upcomingNextPage = it + 1
        }
    }
}