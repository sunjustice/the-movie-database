package com.kiandigital.domain.result

import com.kiandigital.domain.models.Movie

sealed class MovieResult: Result {
    object Loading: MovieResult()
    object LoadingMore: MovieResult()
    data class Success(val movies: List<Movie>): MovieResult()
    data class Failure(val errorMessage: String): MovieResult()
}