package com.kiandigital.domain.di

import com.kiandigital.domain.usecase.*
import org.koin.dsl.module

val domainModule = module {
    single { GetPopularMovieUseCase(tmdbRepositoryContract = get()) }
    single { LoadMorePopularMovieUseCase(tmdbRepositoryContract = get()) }
    single { GetTopRatedMovieUseCase(tmdbRepositoryContract = get()) }
    single { LoadMoreTopRatedMovieUseCase(tmdbRepositoryContract = get()) }
    single { GetUpcomingMovieUseCase(tmdbRepositoryContract = get()) }
    single { LoadMoreUpcomingMovieUseCase(tmdbRepositoryContract = get()) }
}