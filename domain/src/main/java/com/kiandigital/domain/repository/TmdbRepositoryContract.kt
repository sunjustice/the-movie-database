package com.kiandigital.domain.repository

import com.kiandigital.domain.models.Movie

interface TmdbRepositoryContract {
    suspend fun getPopularMoviesAsync(loadMore: Boolean): List<Movie>?
    suspend fun getTopRatedMoviesAsync(loadMore: Boolean): List<Movie>?
    suspend fun getUpcomingMoviesAsync(loadMore: Boolean): List<Movie>?
}