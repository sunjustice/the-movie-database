package com.kiandigital.domain.usecase

import com.kiandigital.domain.repository.TmdbRepositoryContract
import com.kiandigital.domain.result.MovieResult

class GetTopRatedMovieUseCase(private val tmdbRepositoryContract: TmdbRepositoryContract): BaseMovieUseCase() {

    override suspend fun getMovieAsync() = tmdbRepositoryContract.getTopRatedMoviesAsync(loadMore = false)

    override val loadingResult = MovieResult.Loading
}