package com.kiandigital.domain.usecase

import com.kiandigital.domain.repository.TmdbRepositoryContract
import com.kiandigital.domain.result.MovieResult

class LoadMoreTopRatedMovieUseCase(private val tmdbRepositoryContract: TmdbRepositoryContract): BaseMovieUseCase() {

    override suspend fun getMovieAsync() = tmdbRepositoryContract.getTopRatedMoviesAsync(loadMore = true)

    override val loadingResult = MovieResult.Loading
}