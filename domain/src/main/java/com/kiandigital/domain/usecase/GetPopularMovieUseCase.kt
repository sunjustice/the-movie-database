package com.kiandigital.domain.usecase

import com.kiandigital.domain.repository.TmdbRepositoryContract
import com.kiandigital.domain.result.MovieResult

class GetPopularMovieUseCase(private val tmdbRepositoryContract: TmdbRepositoryContract): BaseMovieUseCase() {

    override suspend fun getMovieAsync() = tmdbRepositoryContract.getPopularMoviesAsync(loadMore = false)

    override val loadingResult = MovieResult.Loading
}