package com.kiandigital.domain.usecase

import com.kiandigital.domain.repository.TmdbRepositoryContract
import com.kiandigital.domain.result.MovieResult

class LoadMoreUpcomingMovieUseCase(private var tmdbRepositoryContract: TmdbRepositoryContract): BaseMovieUseCase() {

    override suspend fun getMovieAsync() = tmdbRepositoryContract.getTopRatedMoviesAsync(loadMore = true)
    override val loadingResult = MovieResult.Loading
}