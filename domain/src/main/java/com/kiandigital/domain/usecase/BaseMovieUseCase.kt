package com.kiandigital.domain.usecase

import androidx.lifecycle.LiveDataScope
import com.kiandigital.domain.models.Movie
import com.kiandigital.domain.result.MovieResult

abstract class BaseMovieUseCase : BaseUseCase<MovieResult>() {

    abstract suspend fun getMovieAsync(): List<Movie>?
    abstract val loadingResult: MovieResult


    override suspend fun execute(scope: LiveDataScope<MovieResult>) {
        scope.emit(loadingResult)
        scope.emit(getMovies())

    }


    private suspend fun getMovies(): MovieResult {
        return try {
            var data = getMovieAsync()
            data = data ?: emptyList()
            MovieResult.Success(data)
        } catch (throwable: Throwable) {
            MovieResult.Failure(throwable.localizedMessage)
        }
    }
}