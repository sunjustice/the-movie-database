package com.kiandigital.domain.usecase

import com.kiandigital.domain.repository.TmdbRepositoryContract
import com.kiandigital.domain.result.MovieResult

class LoadMorePopularMovieUseCase(private val tmdbRepositoryContract: TmdbRepositoryContract): BaseMovieUseCase() {

    override suspend fun getMovieAsync() = tmdbRepositoryContract.getPopularMoviesAsync(loadMore = true)
    override val loadingResult =  MovieResult.Loading
}