package com.kiandigital.themoviedatabase.ui.movies.main

import android.widget.ImageView
import com.kiandigital.domain.models.Movie

interface MovieClickListener {
    fun onMovieClick(movie: Movie, movieImage: ImageView)
}