package com.kiandigital.themoviedatabase.ui.movies.main

import android.widget.ImageView
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.kiandigital.domain.action.MoviesAction
import com.kiandigital.domain.models.Movie

class TopRatedMoviesFragment : MoviesBaseFragment() {

    override fun getMovies() = moviesViewModel.dispatch(MoviesAction.GetTopRatedMoviesAction)

    override fun loadMoreMovies() = moviesViewModel.dispatch(MoviesAction.LoadMoreTopRatedMoviesAction)

    override fun showMovieDetails(movie: Movie, movieImage: ImageView) {
        val directions = TopRatedMoviesFragmentDirections.actionTopRatedMoviesFragmentToMovieDetailFragment(movie)
        val extras = FragmentNavigatorExtras(movieImage to movieImage.transitionName)
        findNavController().navigate(directions, extras)
    }
}
