package com.kiandigital.themoviedatabase

import android.app.Application
import com.kiandigital.data.di.dataModule
import com.kiandigital.domain.di.domainModule
import com.kiandigital.presentation.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class TMDBApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        startKoin {
            androidContext(this@TMDBApplication)
            modules(listOf(presentationModule, domainModule, dataModule))
        }
    }
}